import argparse
from kiwi_fruit_detection.input_pipeline.run_input_pipeline import RunInputPipeline
from kiwi_fruit_detection.utils.draw_bbox_test import DrawBoundingBox

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-r', action='store_true', default=False,
                    dest='run_pipeline',
                    help='Run input pipeline')
parser.add_argument('-d', action='store_true', default=False,
                    dest='draw',
                    help='Draw Bounding Box on an image')


if __name__ == '__main__':

    args = parser.parse_args()

    if not args.run_pipeline and not args.draw:
        print("Please select a option. Run with -h argument for details.")

    if args.run_pipeline and args.draw:
        print("Please select one option at a time")

    if args.run_pipeline :
        rip = RunInputPipeline()
        rip.run()

    if args.draw:
        dbb = DrawBoundingBox()
        dbb.draw_bounding_box()