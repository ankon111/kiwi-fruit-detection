## KIWI FRUIT DETECTION

The purpose of this project is to train a object detection model which can then detect kiwi and extract
kiwi from an image and save those images into a folder.

## Installation
Install miniconda from [here](https://docs.conda.io/en/latest/miniconda.html)

Then run following command to create conda environment:
```
conda env create -f environment.yml
```
For model training I used darknet YOLOv4 model. For model training we need to setup darknet properly.
To build properly in respective OS please follow this [instructions](https://github.com/AlexeyAB/darknet#using-also-powershell).
I build darknet in Windows OS. For linux they also have details description how to build and use darknet.

### Problem Identification

Let's take a look at our dataset:

![Training data](./kiwi_fruit_detection/assets/14_100.jpg)
![Validation data](./kiwi_fruit_detection/assets/r_50_100.jpg)
<img src="./kiwi_fruit_detection/assets/kiwis-test-3.jpeg" alt="isolated" width="150"/>

From the left to right we have training data, we have validation data and finally we have our evaluation data.
From the image it's quite obvious that our training and validation data are quite similar whereas evaluation data is 
quite different in terms of image size, scale, different background etc. By training on this dataset we need to identify 
kiwi on the evaluation dataset which is our goal. In order to detect properly we need to prepare our data cautiously.

### Dataset Size
Training Data: 466
Validation Data: 166

### Data Preparation

For object detection, we need to annotate our dataset with bounding box. A bounding bos is the coordinates which defines
where is object is located in the image. Now if we want to annotate this data by hand it won't be feasible. Moreover, 
if we draw the bounding box on such a small image the model will fail to detect the real world data(evaluation dataset).

Using background reduction we can use this image as our annotation and this whole process can be done programmatically.
Let's go through each of the step of our data processing pipeline and understand how it works:

1. **Image resizing:** We resize 100 training image and 30 validation in this shape (50, 50), (70, 70), (80, 80), (120, 120),
(150, 150), (180, 180), (200, 200), (250, 250), (300, 300) randomly. We do such resizing so that model can deal with 
different sizes instead of a static size of object. 
The output looks like this: 

![original](./kiwi_fruit_detection/assets/r_225_100.jpg?raw=true)
![resized](./kiwi_fruit_detection/assets/resize_r_225_100.jpg?raw=true)

2. **Random Augmentation:** We did some random augmentation on training data only. Following augmentation,
we used on 60% of training data. Augmentation helps with data variability that helps model to learn better.
We applied two random augmentation on random data. We used following augmentation technique:

* blur
* horizontal_flip
* translate_X
* rotate 
* contrast
 
![](./kiwi_fruit_detection/assets/rotate_contrast_r_282_100.jpg)
![](./kiwi_fruit_detection/assets/rotate_translate_X_r_214_100.jpg)
![](./kiwi_fruit_detection/assets/translate_X_blur_134_100.jpg)

Above images are output of some augmentation.

3. **Add Negative Samples:** In order to detect real world data and reduce False Positive
detection we add some negative samples like apple, banana etc.

4. **Background Reduction:** We used OpenCV and remove unnecessary white background. We did on purpose, so that the 
output image height, width can act as bounding box. How we achieved that we will discuss later. Let's see output of the 
background reduction: 

![](./kiwi_fruit_detection/assets/0_100.jpg)
![](./kiwi_fruit_detection/assets/bg_reduction_0_100.jpg)

From the images above we can say that, if we draw a bounding box on original image(left one) then the output will look like the 
image(right one) and we achieved that by background reduction.

4. **Transparent Background**: For the next step we remove the white background and make it transparent so that it will
help our final step which is creating collage. However, image with white border surrounding always may make our model 
confused and model could be looking for that only. That's why we make that transparent so that when we will add multiple
image to build our dataset and one will overlap on another then the transparent border helps to see the bottom image.

![](./kiwi_fruit_detection/assets/0_100.jpg)
![](./kiwi_fruit_detection/assets/transparent_0_100.png)

6. **Collage:** For creating collage we take a white background and create positive samples and negative samples 
randomly. In order to create one collage image and bounding box(bbox) we did following:

* take random coordinates(x, y) (act as starting coordinate of bbox)
* take random images
* paste the image on the background (how many image will be added that is also random)
* calculate bbox from starting coordinates and image width and height
* calculate yolo format bbox which is (x-center, y-center, img_width, img_height)
* write down the bbox in a text file
* save the collage image in the end

The output of a collage looks like following:

<img src="./kiwi_fruit_detection/assets/train_img_3.png" alt="isolated" width="200"/>
<img src="./kiwi_fruit_detection/assets/train_img_18.png" alt="isolated" width="200"/>

Image from right is considered as negative sample, in such case there is no bounding box. This helps to reducing false
positives. After finishing collage we checked whether is our bbox coordinates calculated properly or not.

<img src="./kiwi_fruit_detection/assets/kiwi_collage_bbox.jpg" alt="isolated" width="200"/>

Bbox calculated perfectly. Now everything is set!!! 

### How to run input pipeline

In `utils.config` we set all the paths. We configure the total collage image we need and
number of augmented files we will generate. 


Then we need to copy our training and validation dataset in the data folder. Our data folder should looks
like this:
```
kiwi_fruit_detection
|
└───data
│   │   noise
│   │   training (keep the training data here)
│   │   validation (keep the validation data here)
│   │   background.jpg
│   │   background.png
│   │   
```
After that we need to execute 
```
python main.py -r
``` 

This will generate necessary files and our finalized training and validation dataset.
We will find our finalized data in `input_data->final_data` directory. The directory should look like this:
```
kiwi_fruit_detection
|
└───data
│   │   training 
│   │   validation
└───input_data
│   │   augmented
│   │   bg_reduction
└───────final_data
│   │      -training (data we will use for training)
│   │      -validation (data we will use for training)      
│   │      -train.txt (data we will use for training)
│   │      -valid.txt (data we will use for training)
```

**Note:**
If you want to test your model performance (after training) you can take some images from valid folder to another folder and also remember to
remove the image reference from valid.txt. I did not do this programmatically because our objective is to detect kiwi 
on the evaluation dataset. 
 
### Training YOLOV4 model

**Here we have some manual copy-paste operation as darknet official does not provide any python 
API to train the model. There are some third party implementation in python but not efficient as much as official.**

After installing darknet properly we should see the darknet folder like this:

<img src="./kiwi_fruit_detection/assets/darknet.png" alt="isolated" width="300"/>

Now for training we need to do following step:

1. copy training folder content (image and text files) and put into `darknet/data/obj/` directory.
2. copy validation folder content (image and text files) and put into `darknet/data/obj/` directory.
3. go to `input_data` directory and copy `train.txt` and `valid.txt` in `darknet\data\` directory
4. modify `darknet\data\obj.data` file with following content
```
classes = 1
train = data/train.txt
valid = data/valid.txt
names = data/obj.names
backup = backup
```
5. modify `darknet\data\obj.names` file with following:
```
kiwi
```
6. copy [custom-yolov4-detector.cfg](kiwi_fruit_detection/assets/custom-yolov4-detector.cfg) file and paste into
`darknet/cfg/` directory.
7. We want to use some pre-trained weights for faster convergence. Please download 
[yolov4.conv.137](https://drive.google.com/file/d/1xM96rmb4BmQsFqoi7ZOxfCl3vzn32D9z/view?usp=sharing) and put the weight 
file into `darknet/` directory.

Thats it!! We are ready for training.

**Training command:**

```
.\darknet.exe detector train data/obj.data .\cfg\yolov4-custom.cfg yolov4.conv.137 -dont_show -mjpeg_port 8090 -map
```

We can see the progress of training using `localhost:8090` and this will looks like this:

<img src="./kiwi_fruit_detection/assets/chart.png" alt="isolated" width="400"/>

**Model Performance**

In object detection model performance is measured using [mAP(mean average precision)](https://towardsdatascience.com/breaking-down-mean-average-precision-map-ae462f623a52#1a59).

* mAP = 98.8%
* loss = 0.6942

We run the training for 4000 iteration, however we took the above picture after 2000 iteration.

**Inference on Evaluation Dataset**

I create dataset in different combinations and find best model from that. Combinations are like:

1. resize + augmentation + background_reduction + transparent background+ with noise 
2. resize + augmentation + background_reduction + without noise
3. augmentation + background reduction + transparent background + with noise (**best model**)
4. etc...

From above no 3. configuration gives us the best results. We can download the best 
[final.weights](https://drive.google.com/file/d/1s2-LNEhyeDoRV1FMzz-lnTxF8xr_elCf/view?usp=sharing) and use following 
command for inference:

```
.\darknet.exe detector test data/obj.data .\cfg\custom-yolov4-detector.cfg final.weights -thresh 0.5 -ext_output x_test/kiwis-test-2.jpg
```

The output looks like:

<img src="./kiwi_fruit_detection/assets/kiwis-1.png" alt="isolated" width="416"/>
<img src="./kiwi_fruit_detection/assets/kiwis-2.png" alt="isolated" width="416"/>
<img src="./kiwi_fruit_detection/assets/kiwis-3.png" alt="isolated" width="416"/>

From the output we can see that most of the kiwis are detected with high confidence but there is also some false 
positives as well.

**Save Predicted Coordinates**

In order to cut the kiwi (patching) from the images we need to save the coordinates from our prediction. Following steps needs to 
be done to do so:

1. copy `evaluation` dataset folder in `darknet/data/`
2. create a text file `data/pred.txt` which contains the image path of evaluation folder. The content of the text file 
looks like following:
```
data/evaluation/kiwis-test-1.jpg
data/evaluation/kiwis-test-2.jpg
data/evaluation/kiwis-test-3.jpeg
```
Run following command to get the coordinates:

```
.\darknet detector test data/obj.data .\cfg\custom-yolov4-detector.cfg final.weights -thresh 0.5 -dont_show -save_labels < data\pred.txt
```

By running this command the model will predict and save the coordinates in the `data/evaluation/` directory. 

**Save each kiwi in a directory**

To do so we need to copy the `darknet/data/evaluation` and paste into `kiwi-fruit-detection/kiwi_fruit_detection/data/` 
directory. Alternatively here is the [download link](https://drive.google.com/file/d/17M4YYxFB5TBHTR2ivNWx0X6HExmRVZYJ/view?usp=sharing)
for the evaluation folder which I generated. Then run the following script:
```
python main.py -d
```

This will save the extracted patch into the `data/extract/` directory. Here is the [download link](https://drive.google.com/file/d/1zzjBv65BEf9bh6TKmXxaBBiNI_ly4fRe/view?usp=sharing) 
of patch folder that I generated. Sample patch is given below:

<img src="./kiwi_fruit_detection/assets/kiwi_patch_5.jpg" alt="isolated" width="150"/>
<img src="./kiwi_fruit_detection/assets/kiwi_patch_15.jpg" alt="isolated" width="150"/>
<img src="./kiwi_fruit_detection/assets/kiwi_patch_18.jpg" alt="isolated" width="150"/>

### Improvements

1. The model I trained that still detect false positives. We need to do research what are the alternative options beside 
using negative samples.
2. Although Darknet YOLOV4 model is giving good results, but we need to find some python implementation that can ease 
our training procedure.

### Future Task
1. Implement TensorFlow object detection API.
2. Implement YOLOV4 python implementation.
