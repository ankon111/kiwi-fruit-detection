import glob
import os
from pathlib import Path

import cv2
import numpy as np
from tqdm import tqdm

from kiwi_fruit_detection.utils import config


class BackGroundReduction:

    def __init__(self, data_path, dest_path):
        self.data_path = data_path
        self.dest_path = dest_path

    def background_reduction(self):

        # threshold on background color
        lowerBound = (230, 230, 230)
        upperBound = (255, 255, 255)
        kernel_7 = np.ones((7, 7), np.uint8)
        kernel_11 = np.ones((11, 11), np.uint8)
        path = os.path.join(self.data_path, '*')
        image_path_list = glob.glob(path)

        for image_path in tqdm(image_path_list):
            base_name = os.path.basename(image_path)
            image_path_out = os.path.join(self.dest_path, base_name)
            img = cv2.imread(image_path)

            thresh = cv2.inRange(img, lowerBound, upperBound)
            # invert so background black
            thresh = 255 - thresh

            # apply morphology to ensure regions are filled and remove extraneous noise

            thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel_7)
            thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel_11)

            # get contours
            contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours = contours[0] if len(contours) == 2 else contours[1]

            # get bounding boxes and crop input
            for cntr in contours:
                # get bounding boxes
                x, y, w, h = cv2.boundingRect(cntr)
                crop = img[y:y + h, x:x + w]
                cv2.imwrite(image_path_out, crop)


if __name__ == '__main__':
    bgr = BackGroundReduction(data_path=config.training_augmented_dest_path,
                              dest_path=config.training_bg_reduction_path)
    bgr.background_reduction()