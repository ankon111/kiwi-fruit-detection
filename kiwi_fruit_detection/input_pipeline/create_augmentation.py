import glob
import os.path
from pathlib import Path

import cv2
import numpy as np
import random
import imgaug.augmenters as iaa

# image processing library
# from skimage import io
from kiwi_fruit_detection.utils import config


class RandomAugmentation:

    def __init__(self):
        # dictionary of the transformations we defined earlier
        self.available_transformations = {
            'blur': self.blur_image,
            'horizontal_flip': self.horizontal_flip,
            'translate_X': self.translate_X,
            'rotate': self.rotate_90,
            'contrast': self.contrast
        }

        self.training_data_path = config.training_data_path
        self.training_augmented_dest_path = config.training_augmented_dest_path

    def remove_black_pixel(self, image_array):
        # get (i, j) positions of all RGB pixels that are black (i.e. [0, 0, 0])
        black_pixels = np.where(
            (image_array[:, :, 0] == 0) &
            (image_array[:, :, 1] == 0) &
            (image_array[:, :, 2] == 0)
        )

        # set those pixels to white
        image_array[black_pixels] = [255, 255, 255]
        return image_array

    def translate_X(self, image_array):
        aug = iaa.Affine(translate_percent={"x": -0.20})
        images_aug = aug(image=image_array)
        return self.remove_black_pixel(images_aug)

    def contrast(self, image_array):
        aug = iaa.GammaContrast((0.5, 2.0))
        images_aug = aug(image=image_array)
        return images_aug

    def rotate_90(self, image_array):
        aug = iaa.Rot90(1)
        images_aug = aug(image=image_array)
        return self.remove_black_pixel(images_aug)

    def random_noise(self, image_array):
        aug = iaa.AdditiveGaussianNoise(scale=(0, 0.2 * 255))
        images_aug = aug(image=image_array)
        return images_aug

    def horizontal_flip(self, image_array):
        # horizontal flip doesn't need skimage, it's easy as flipping the image array of pixels !
        aug = iaa.Flipud(1)
        images_aug = aug(image=image_array)
        return images_aug

    def blur_image(self, image_array):
        aug = iaa.GaussianBlur(sigma=(1.0, 3.0))
        images_aug = aug(image=image_array)
        return images_aug

    def get_image(self, images, aug_applied):
        check = True
        while check:
            img = random.choice(images)
            if img not in aug_applied:
                check = False
        return img

    def random_key(self, key_list):
        check = True
        while check:
            key = random.choice(list(self.available_transformations))
            if key not in key_list:
                check = False
        return key

    def random_augmentation(self):

        num_files_desired = config.num_of_augmented_files_desired
        num_generated_files = 0
        path = os.path.join(self.training_data_path, '*')
        images = glob.glob(path)
        aug_applied = []

        while num_generated_files <= num_files_desired:
            # random image from the folder
            image_path = self.get_image(images, aug_applied)
            aug_applied.append(image_path)
            # read image as an two dimensional array of pixels
            image_to_transform = cv2.imread(image_path)
            # image_to_transform = image_to_transform[:, :, ::-1]
            transformed_image = image_to_transform.copy()
            key_list = []
            for i in range(2):
                # random transformation to apply for a single image
                key = self.random_key(key_list)
                key_list.append(key)
                transformed_image = self.available_transformations[key](transformed_image)
            key = '_'.join(key_list)
            image_out_name = f"{key}_{os.path.basename(image_path)}"
            transformed_image_path = os.path.join(self.training_augmented_dest_path, image_out_name)

            # write image to the disk
            cv2.imwrite(transformed_image_path, transformed_image)
            num_generated_files += 1


if __name__ == '__main__':
    ra = RandomAugmentation()
    ra.random_augmentation()


