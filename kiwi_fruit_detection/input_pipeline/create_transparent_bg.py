import os
import glob
from PIL import Image
from pathlib import Path
from tqdm import tqdm

from kiwi_fruit_detection.utils import config


class TransparentBackGround:

    def __init__(self, data_path, dest_path):
        self.data_path = data_path
        self.dest_path = dest_path

    def create_transparent_bg_image(self):
        path = os.path.join(self.data_path, '*')
        image_list = glob.glob(path)

        for image_path in tqdm(image_list):
            img = Image.open(image_path)
            img = img.convert("RGBA")
            datas = img.getdata()
            newData = []
            for item in datas:
                if item[0] > 240 and item[1] > 240 and item[2] > 240:
                    newData.append((255, 255, 255, 0))
                else:
                    newData.append(item)
            img.putdata(newData)
            file_name = f"{Path(image_path).stem}.png"
            image_path_out = os.path.join(self.dest_path, file_name)
            img.save(image_path_out, format="PNG")


if __name__ == '__main__':
    tbg = TransparentBackGround(data_path=config.negative_data_path,
                                dest_path=config.negative_transparent_data_path
                                )
    tbg.create_transparent_bg_image()
