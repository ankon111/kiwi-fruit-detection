import os
import glob
import shutil
import sys

from kiwi_fruit_detection.input_pipeline.background_removal import BackGroundReduction
from kiwi_fruit_detection.input_pipeline.create_augmentation import RandomAugmentation
from kiwi_fruit_detection.input_pipeline.create_collage import CollageCreation
from kiwi_fruit_detection.input_pipeline.create_transparent_bg import TransparentBackGround
from kiwi_fruit_detection.input_pipeline.resize_training_data import ResizeData
from kiwi_fruit_detection.input_pipeline.yolo_text_file import CreateTextFile
from kiwi_fruit_detection.utils import config


class RunInputPipeline:

    def remove_files(self, path):
        filelist = glob.glob(os.path.join(path, "*"))
        for f in filelist:
            os.remove(f)

    def create_directories(self):
        list_dir = [config.negative_transparent_data_path,
                    config.training_augmented_dest_path,
                    config.training_bg_reduction_path,
                    config.validation_bg_reduction_path,
                    config.training_transparent_path,
                    config.validation_transparent_path,
                    config.final_training_data_path,
                    config.final_validation_data_path
                    ]

        for dir in list_dir:
            if os.path.exists(dir):
                self.remove_files(dir)
            else:
                os.makedirs(dir, exist_ok=True)

    def run(self):

        # Remove old files and creating dir
        print("Remove old data and creating directories.....")
        self.create_directories()
        print("Directory operation finished.....")

        if config.do_resize:
            print("Resizing training data.....")
            rt = ResizeData(config.training_data_path, config.training_data_path, no_resize_image=100)
            rt.resize_data()
            print("Resizing finished.....")

            print("Resizing training data.....")
            rt = ResizeData(config.validation_data_path, config.validation_data_path, no_resize_image=30)
            rt.resize_data()
            print("Resizing finished.....")

        print("Augmentation started.....")
        ra = RandomAugmentation()
        ra.random_augmentation()
        print("Augmentation finished.....")

        print("Copying original training data to augmentation data for bg reduction......")
        # moving all the original training image to augmented image folder for bg reduction
        for filename in glob.glob(os.path.join(config.training_data_path, '*.*')):
            shutil.copy(filename, config.training_augmented_dest_path)

        print("File copying finished......")

        print("Background reduction started for training dataset.....")
        bgr = BackGroundReduction(data_path=config.training_augmented_dest_path,
                                  dest_path=config.training_bg_reduction_path)
        bgr.background_reduction()
        print("Background reduction finished.....")

        print("Background reduction started for validation dataset.....")
        bgr = BackGroundReduction(data_path=config.validation_data_path,
                                  dest_path=config.validation_bg_reduction_path)
        bgr.background_reduction()
        print("Background reduction finished.....")

        #
        print("Creating transparent Background for negative data.....")
        tbg = TransparentBackGround(data_path=config.negative_data_path,
                                    dest_path=config.negative_transparent_data_path
                                    )
        tbg.create_transparent_bg_image()
        print("Transparent Background created")

        print("Creating transparent Background for training data.....")
        tbg = TransparentBackGround(data_path=config.training_bg_reduction_path,
                                    dest_path=config.training_transparent_path
                                    )
        tbg.create_transparent_bg_image()
        print("Transparent Background created")

        print("Creating transparent Background for validation data.....")
        tbg = TransparentBackGround(data_path=config.validation_bg_reduction_path,
                                    dest_path=config.validation_transparent_path
                                    )
        tbg.create_transparent_bg_image()
        print("Transparent Background created")

        print("Collage image creation for training data.....")
        cc = CollageCreation(data_path=config.training_transparent_path,
                             dest_path=config.final_training_data_path,
                             negative_data_path=config.negative_transparent_data_path,
                             total_collage_image=config.total_training_collage_image,
                             data_format=config.data_format,
                             data_type='train')

        cc.create_collage()
        print("Collage Image creation finished.....")

        print("Collage image creation for validation data.....")
        cc = CollageCreation(data_path=config.validation_transparent_path,
                             dest_path=config.final_validation_data_path,
                             negative_data_path=config.negative_transparent_data_path,
                             total_collage_image=config.total_validation_collage_image,
                             data_format=config.data_format,
                             data_type='valid')

        cc.create_collage()
        print("Collage Image creation finished.....")

        if config.data_format == 'yolo':
            # For training

            ctxf = CreateTextFile(config.final_training_data_path,
                                  config.yolo_train_text_file,
                                  config.darknet_path
                                  )
            ctxf.create_yolo_text_file()
            # For validation
            ctxf = CreateTextFile(config.final_validation_data_path,
                                  config.yolo_valid_text_file,
                                  config.darknet_path
                                  )
            ctxf.create_yolo_text_file()
