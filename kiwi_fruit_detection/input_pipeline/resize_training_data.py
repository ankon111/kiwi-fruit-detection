import os
import glob
import random
import cv2
from random import shuffle

from kiwi_fruit_detection.utils import config


class ResizeData:

    def __init__(self, data_path, dest_path, no_resize_image=150):
        self.data_path = data_path
        self.dest_path = dest_path
        self.default_resize_list = [(50, 50), (70, 70), (80, 80), (120, 120),
                                    (150, 150), (180, 180), (200, 200),
                                    (250, 250), (300, 300)]
        self.no_resize_image = no_resize_image

    def resize_data(self):
        path = os.path.join(self.data_path, '*')
        image_list = glob.glob(path)
        shuffle(image_list)

        for i in range(self.no_resize_image):
            size = random.choice(self.default_resize_list)
            image_path = random.choice(image_list)
            img = cv2.imread(image_path)
            img = cv2.resize(img, size)

            img_name = f"resize_{os.path.basename(image_path)}"
            file_path = os.path.join(self.dest_path, img_name)
            cv2.imwrite(file_path, img)


if __name__ == '__main__':
    rt = ResizeData(config.training_data_path, config.export_path, no_resize_image=1)
    rt.resize_data()