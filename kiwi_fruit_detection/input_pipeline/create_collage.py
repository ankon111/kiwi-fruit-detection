import glob
import os.path
import random
from random import shuffle
from PIL import Image

from kiwi_fruit_detection.utils import config


class CollageCreation:

    def __init__(self, data_path,
                 dest_path,
                 negative_data_path,
                 total_collage_image,
                 data_format,
                 data_type
                 ):
        self.data_path = data_path
        self.dest_path = dest_path
        self.negative_data_path = negative_data_path
        self.data_format = data_format
        self.total_collage_image = total_collage_image
        self.data_type = data_type

    def yolo_format(self, x, y, w, h, bg_w, bg_h):
        """ x_center,y_center, width, height

        :return:
        """
        x_center_norm = (x + w / 2) / bg_w
        y_center_norm = (y + h / 2) / bg_h
        w_norm = w / bg_w
        h_norm = h / bg_h

        return x_center_norm, y_center_norm, w_norm, h_norm

    def overlay_image(self, background_image, image_path, bg_w, bg_h):
        x = random.randint(50, bg_w - 50)
        y = random.randint(50, bg_h - 50)
        overlay_image = Image.open(image_path)
        w, h = overlay_image.size
        background_image.paste(overlay_image, (x, y), mask=overlay_image)

        return background_image, x, y, w, h

    def add_negative_example(self, background_image, negative_image_list, num_img_add, bg_w, bg_h):
        if num_img_add > 0:
            negative_sub_image_list = random.choices(negative_image_list, k=num_img_add)
            for image_path in negative_sub_image_list:
                background_image, _, _, _, _ = self.overlay_image(background_image, image_path, bg_w, bg_h)

        return background_image

    def create_collage(self):

        path = os.path.join(self.data_path, '*')
        image_list = glob.glob(path)
        shuffle(image_list)

        path = os.path.join(self.negative_data_path, '*')
        negative_image_list = glob.glob(path)
        shuffle(negative_image_list)

        count = 0
        idx = 0
        white_background = True
        while count < self.total_collage_image:
            # if random.randint(1, 10) == 10:
            #     bg_img = Image.open(config.bg_img_new_path)
            #     if self.data_type == 'train':
            #         bg_img = bg_img.rotate(random.choice([0, 90, 180, 270]))
            #     white_background = False
            # else:
            #     bg_img = Image.open(config.white_bg_img_path)
            #     white_background = True

            bg_img = Image.open(config.white_bg_img_path)
            bg_w, bg_h = bg_img.size
            background_image = Image.new('RGBA', (bg_w, bg_h), (0, 0, 0, 0))
            background_image.paste(bg_img, (0, 0))

            file_name = f"{self.data_type}_img_{count}"
            text_file_path = os.path.join(self.dest_path, f"{file_name}.txt")

            if random.randint(1, 7) > 5 and white_background:
                n = random.randint(1, 10)
                background_image = self.add_negative_example(background_image,
                                                             negative_image_list,
                                                             num_img_add=n,
                                                             bg_w=bg_w,
                                                             bg_h=bg_h)
                if self.data_format == "yolo":
                    with open(text_file_path, 'w') as file:
                        file.write("")

            else:
                # add negative example or not with positive sample
                n = 0
                if white_background:
                    n = random.randint(0, 2)
                    background_image = self.add_negative_example(background_image,
                                                                 negative_image_list,
                                                                 num_img_add=n,
                                                                 bg_w=bg_w,
                                                                 bg_h=bg_h)

                # Number of train/valid image add in one canvas
                k = random.randint(1, 10 - n)

                if idx + k > len(image_list):
                    sub_image_list = image_list[idx:]
                    shuffle(image_list)
                    rest_idx = k - len(sub_image_list)
                    sub_image_list.extend(image_list[:rest_idx])
                else:
                    sub_image_list = image_list[idx:idx + k]

                coord_list = []
                x_center_norm, y_center_norm, w_norm, h_norm = 0, 0, 0, 0
                for image_path in sub_image_list:
                    background_image, x, y, w, h = self.overlay_image(background_image, image_path, bg_w, bg_h)
                    # print(x, y , x+w , y+h)
                    # yolo format
                    if self.data_format == 'yolo':
                        x_center_norm, y_center_norm, w_norm, h_norm = self.yolo_format(x, y, w, h, bg_w, bg_h)
                    coord_list.append([0, x_center_norm, y_center_norm, w_norm, h_norm])

                with open(text_file_path, 'a') as file:
                    for coord in coord_list:
                        file.write(f"{coord[0]} {coord[1]} {coord[2]} {coord[3]} {coord[4]}\n")

                idx = idx + k
            # common for all
            img_file_path = os.path.join(self.dest_path, f"{file_name}.png")
            background_image.save(img_file_path, format="PNG")

            count += 1


if __name__ == '__main__':
    cc = CollageCreation(data_path=config.validation_transparent_path,
                         dest_path=config.final_validation_data_path,
                         negative_data_path=config.negative_transparent_data_path,
                         total_collage_image=50,
                         data_format='yolo',
                         data_type='valid')

    cc.create_collage()
