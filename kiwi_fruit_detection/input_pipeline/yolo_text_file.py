import os
import glob
from kiwi_fruit_detection.utils import config


class CreateTextFile:
    def __init__(self, src_path, text_file, darknet_path):
        self.src_path = src_path
        self.text_file = text_file
        self.darknet_path = darknet_path

    def create_yolo_text_file(self):
        path = os.path.join(self.src_path, "*.png")
        image_list = glob.glob(path)

        with open(self.text_file, "w") as file:
            for image in image_list:
                img_name = os.path.basename(image)
                path = os.path.join(self.darknet_path, img_name)
                file.write(f"{path}\n")


if __name__ == '__main__':
    # For training

    ctxf = CreateTextFile(config.final_training_data_path,
                          config.yolo_train_text_file,
                          config.darknet_path
                          )
    ctxf.create_yolo_text_file()

    # For validation
    ctxf = CreateTextFile(config.final_validation_data_path,
                          config.yolo_valid_text_file,
                          config.darknet_path
                          )
    ctxf.create_yolo_text_file()
