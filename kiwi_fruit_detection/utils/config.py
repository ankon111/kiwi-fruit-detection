import os
from pathlib import Path


def get_joined_path(path):
    return os.path.join(Path(__file__).parent.parent, path)


evaluation_data_path = get_joined_path('data/evaluation/')
training_data_path = get_joined_path('data/training/kiwi/')
validation_data_path = get_joined_path('data/validation/kiwi/')

training_augmented_dest_path = get_joined_path('input_data/augmented/training/kiwi/')

training_bg_reduction_path = get_joined_path('input_data/bg_reduction/training/kiwi/')
validation_bg_reduction_path = get_joined_path('input_data/bg_reduction/validation/kiwi/')

training_transparent_path = get_joined_path('input_data/transparent/training/')
validation_transparent_path = get_joined_path('input_data/transparent/validation/')

white_bg_img_path = get_joined_path('data/background.png')
bg_img_new_path = get_joined_path('data/bg_new.png')

final_training_data_path = get_joined_path('input_data/final_data/training/')
final_validation_data_path = get_joined_path('input_data/final_data/validation/')


negative_data_path = get_joined_path('data/noise/')
negative_transparent_data_path = get_joined_path('data/transparent_noise/')

export_path = get_joined_path('data/export/')

# add resize to input data pipeline
do_resize = False

# total files we want to generate
num_of_augmented_files_desired = 300
total_training_collage_image = 500
total_validation_collage_image = 50

# yolo specific config
data_format = 'yolo'
darknet_path = 'data/obj/'
yolo_train_text_file = get_joined_path('input_data/final_data/train.txt')
yolo_valid_text_file = get_joined_path('input_data/final_data/valid.txt')
