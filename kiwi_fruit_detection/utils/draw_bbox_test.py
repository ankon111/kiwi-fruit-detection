import glob
import math
import os.path
from pathlib import Path
import cv2

from kiwi_fruit_detection.utils import config


class DrawBoundingBox:

    def yolobbox2bbox(self, x, y, w, h):
        x1, y1 = x - w / 2, y - h / 2
        x2, y2 = x + w / 2, y + h / 2
        return math.ceil(x1), math.ceil(y1), math.ceil(x2), math.ceil(y2)

    def draw_bounding_box(self):
        path = os.path.join(config.evaluation_data_path, '*.jp*')
        image_list = glob.glob(path)
        text_file_list = glob.glob(os.path.join(config.evaluation_data_path, '*.txt'))

        if len(image_list) > 1:
            for image_path in image_list:
                img_base_name = os.path.basename(image_path)
                stem = Path(image_path).stem
                text_file_path = next((s for s in text_file_list if stem in s), None)

                patch_path = os.path.join(config.export_path, stem)
                os.makedirs(patch_path, exist_ok=True)

                image = cv2.imread(image_path)
                bg_h, bg_w = image.shape[0], image.shape[1]
                coord_list = []
                with open(text_file_path, 'r') as file:
                    coord_list = file.readlines()

                for idx, coord in enumerate(coord_list):
                    coord = [float(c) for c in coord.strip().split(" ")]
                    x, y, w, h = coord[1] * bg_w, coord[2] * bg_h, coord[3] * bg_w, coord[4] * bg_h
                    x1, y1, x2, y2 = self.yolobbox2bbox(x, y, w, h)

                    x1 = max(0, x1)
                    y1 = max(0, y1)
                    # Saving patch
                    w, h = math.ceil(w), math.ceil(h)
                    patch = image[y1:y1+h, x1:x1+h].copy()
                    patch_file_name = os.path.join(patch_path, f"kiwi_patch_{idx}.jpg")
                    cv2.imwrite(patch_file_name, patch)

                    # Drawing bbox
                    # image = cv2.rectangle(image, (x1, y1), (x2, y2), (36, 255, 12), 2)

                # Saving bbox images
                # img_base_name = f"bbox_{img_base_name}"
                # image_out_path = os.path.join(config.export_path, img_base_name)
                # cv2.imwrite(image_out_path, image)
        else:
            print("No image exists in data/evaluation directory")


if __name__ == '__main__':
    dbb = DrawBoundingBox()
    dbb.draw_bounding_box()